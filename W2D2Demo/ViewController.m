//
//  ViewController.m
//  W2D2Demo
//
//  Created by James Cash on 07-05-19.
//  Copyright © 2019 Occasionally Cogent. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (nonatomic,weak) UIView *orangeView;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    UIView *view1 = [[UIView alloc] initWithFrame:CGRectMake(20, 20, 200, 150)];
    view1.backgroundColor = [UIColor cyanColor];
    [self.view addSubview:view1];

    UIView *view2 = [[UIView alloc] initWithFrame:CGRectMake(10, 10, 220, 170)];
    view2.backgroundColor = [UIColor magentaColor];
    [self.view insertSubview:view2 belowSubview:view1];


    UIView *view3 = [[UIView alloc] initWithFrame:CGRectMake(50, 200, 200, 200)];
    view3.backgroundColor = [UIColor orangeColor];
    [self.view addSubview:view3];

    UIView *view4 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 75, 75)];
    view4.backgroundColor = [UIColor purpleColor];
    [view3 addSubview:view4];
    view4.layer.cornerRadius = 20.0;

    view3.clipsToBounds = NO;
    // what I wish I could do:
//    view3.bounds.origin.y -= 10;
// ugly approach:
//    CGRect newBounds = view3.bounds;
//    newBounds.origin.y -= 10;
//    view3.bounds = newBounds;
// optimal approach:
    view3.bounds = CGRectOffset(view3.bounds, 0, 10);

    for (UIView *v in @[view1, view2, view3, view4]) {
        NSLog(@"Frame: %@ bounds: %@", NSStringFromCGRect(v.frame), NSStringFromCGRect(v.bounds));
    }

    self.orangeView = view3;

    [UIView animateWithDuration:3
                          delay:0
                        options:UIViewAnimationOptionRepeat | UIViewAnimationOptionAutoreverse
                     animations:^{
                         self.orangeView.frame = CGRectInset(self.orangeView.frame, -10, -10);
                     } completion:^(BOOL finished) {
                         // won't happen
                     }];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
//    [UIView animateWithDuration:2 animations:^{
//        self.orangeView.backgroundColor = [UIColor blueColor];
//        self.orangeView.frame = CGRectInset(self.orangeView.frame, -10, -10);
//    }];
    [UIView animateWithDuration:2.5 delay:1 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        self.orangeView.backgroundColor = [UIColor blueColor];
    } completion:^(BOOL finished) {
        NSLog(@"Animation finished");
        self.orangeView.backgroundColor = [UIColor orangeColor];
    }];
    NSLog(@"Animation started");

    UIView *blah = [[UIView alloc] initWithFrame:CGRectMake(100, 100, 100, 100)];
    blah.backgroundColor = [UIColor blackColor];
    [self.view addSubview:blah];

    [UIView animateWithDuration:3 animations:^{
        blah.alpha = 0;
    } completion:^(BOOL finished) {
        // done
        [blah removeFromSuperview];
    }];
}


@end
