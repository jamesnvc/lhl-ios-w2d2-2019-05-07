//
//  SecondViewController.h
//  W2D2Demo
//
//  Created by James Cash on 07-05-19.
//  Copyright © 2019 Occasionally Cogent. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SecondViewController : UIViewController

@end

NS_ASSUME_NONNULL_END
