//
//  SecondViewController.m
//  W2D2Demo
//
//  Created by James Cash on 07-05-19.
//  Copyright © 2019 Occasionally Cogent. All rights reserved.
//

#import "SecondViewController.h"

@interface SecondViewController ()
@property (nonatomic,weak) NSLayoutConstraint *leadingConstraint;
@end

@implementation SecondViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    UIView *view1 = [[UIView alloc] initWithFrame:CGRectZero];
    view1.translatesAutoresizingMaskIntoConstraints = NO;
    view1.backgroundColor = [UIColor redColor];

    [self.view addSubview:view1];

    NSLayoutConstraint *centerXConstraint =
    [NSLayoutConstraint
     constraintWithItem:view1
     attribute:NSLayoutAttributeCenterX
     relatedBy:NSLayoutRelationEqual
     toItem:self.view
     attribute:NSLayoutAttributeCenterX
     multiplier:1.0 constant:0];

    self.leadingConstraint = [view1.leadingAnchor
                          constraintEqualToAnchor:self.view.leadingAnchor
                          constant:10];
    [NSLayoutConstraint activateConstraints:
     @[centerXConstraint,
       [view1.centerYAnchor constraintEqualToAnchor:self.view.centerYAnchor],
       self.leadingConstraint,
//       [view1.widthAnchor constraintEqualToConstant:150],
       [view1.heightAnchor constraintEqualToConstant:150],
       ]];

}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [UIView animateWithDuration:3 animations:^{
        self.leadingConstraint.constant += 10;
        [self.view layoutIfNeeded];
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
